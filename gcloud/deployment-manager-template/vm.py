COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'

def GenerateConfig(context):
    resources = [{
        'name': context.env['name'],
        'type': 'compute.v1.instance',
        'properties': {  
            'zone': context.properties['zone'],
            'machineType': ''.join([COMPUTE_URL_BASE, 'projects/',
                                    context.env['project'], '/zones/',
                                    context.properties['zone'], '/machineTypes/',
                                    context.properties['machineType']]),
            'disks': [{
                'deviceName': 'boot',
                'type': 'PERSISTENT',
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': ''.join([COMPUTE_URL_BASE, 'projects/',
                                            'debian-cloud/global/',
                                            'images/family/debian-10'])
                }
            }],
            'networkInterfaces': [{
                'network': '$(ref.' + context.properties['network']
                            + '.selfLink)',
                'subnetwork': '$(ref.subnet-1.selfLink)',
                'accessConfigs': [{
                    'name': 'External NAT',
                    'type': 'ONE_TO_ONE_NAT'
                }]
            }]
        }
    }]
    return {'resources': resources}