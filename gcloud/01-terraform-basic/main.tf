# Configure the GCP Provider
provider "google" {
  region = "europe-west3"
  zone   = "europe-west3-c"
}

resource "google_compute_instance" "example" {
  name         = "example"
  machine_type = "f1-micro"
  zone         = "europe-west3-c"

  tags = ["example", "http"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }

  labels = {
    ita_group = "kv-062"
  }
}

