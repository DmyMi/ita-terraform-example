# Terraform example

1. Install [Terraform](https://www.terraform.io/).
2. Go to [Credentials page](https://console.cloud.google.com/apis/credentials/serviceaccountkey) and create a new credentials file for your Service Account (or even create a new service account)
3. Add your the path to GCP credentials as the environment variable `GOOGLE_CREDENTIALS`.
4. Add your GCP Project as environment variable `GOOGLE_PROJECT`
5. Run `terraform plan`.
6. If the plan looks good, run `terraform apply`.

## Cleaning up

To clean up the resources created by these templates, just run `terraform destroy`.