variable "name" {
  description = "The name used to namespace resources created by this module"
}

variable "port" {
  description = "The port the app should listen on for HTTP requests"
}

