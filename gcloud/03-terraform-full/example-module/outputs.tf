# Output the URL of the Compute instance after the templates are applied
output "url" {
  value = "http://${google_compute_instance.example_nodejs_app.network_interface[0].access_config[0].nat_ip}:${var.port}"
}

