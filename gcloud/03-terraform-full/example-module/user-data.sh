#!/bin/bash
# A script that runs the app. It expects certain variables to be set via Terraform interpolation.
# Just an example, i'm lazy don't do this in prod :)
set -e

echo "starting app on port ${port}"
sudo apt-get update
sudo apt-get install curl software-properties-common -y
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt-get update
sudo apt-get install nodejs -y
sudo cat <<__EOF__>/server.js
const http = require('http');

const server = http.createServer((req, res) => {
  const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
  res.writeHead(200, {"Content-Type": "text/plain"});
  res.end("Hello, " + ip + "!\n");
});

server.listen(${port});

console.log("Server running at http://127.0.0.1:8080/");

process.on('SIGINT', function() {
  process.exit();
});
__EOF__
sudo cat <<__EOF__>/etc/systemd/system/nodejs.service
[Unit]
Description=NodeJS App
#Documentation=
After=network.target

[Service]
RestartSec=10
Restart=always

# Main process
ExecStart=/usr/bin/node /server.js

[Install]
WantedBy=multi-user.target
__EOF__
sudo systemctl enable --now nodejs
