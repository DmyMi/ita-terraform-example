resource "google_compute_network" "example_net" {
  name                    = "${var.name}-vpc"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "public" {
  name          = "${var.name}-public-subnet"
  ip_cidr_range = "10.2.0.0/16"
  network       = google_compute_network.example_net.self_link
}

