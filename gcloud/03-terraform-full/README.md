# Terraform Example

1. Install [Terraform](https://www.terraform.io/).
1. Open `vars.tf`, set the environment variables specified at the top of the file, and fill in any other variables that
   don't have a `default`.
1. Run `terraform get`.
1. Run `terraform plan`.
1. If the plan looks good, run `terraform apply`.
1. After the templates have been applied, Terraform will output a URL. Once the server is up and running (which can
   take 1-2 minutes), visit this URL to test the NodeJS app.

## Cleaning up

To clean up the resources created by these templates, just run `terraform destroy`.