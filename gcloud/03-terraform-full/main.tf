# Configure the AWS Provider
provider "google" {
  region = "europe-west3"
  zone   = "europe-west3-c"
}

# This shows an example of how to use a Terraform module.
module "example_nodejs_app" {
  # The source field can be a path on your file system or a Git URL
  source = "./example-module"

  # Pass parameters to the module
  name = var.app_name
  port = 8080
}

