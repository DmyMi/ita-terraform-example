Validate
```bash
aws cloudformation validate-template \
  --template-body file://template.yaml
```

Create stack
```bash
# replace ParameterKey=KeyName,ParameterValue=key with your key, i.e. ParameterKey=KeyName,ParameterValue=dmin-key
aws cloudformation create-stack \
  --template-body file://template.yaml \
  --stack-name MyTemplate \
  --region us-east-1 \
  --parameters ParameterKey=KeyName,ParameterValue=key \
  ParameterKey=EnvType,ParameterValue=dev
```

Get template
```bash
aws cloudformation get-template \
  --stack-name MyTemplate
```

Update stack
```bash
# replace ParameterKey=KeyName,ParameterValue=key with your key, i.e. ParameterKey=KeyName,ParameterValue=dmin-key
aws cloudformation update-stack \
  --stack-name MyTemplate \
  --template-body file://template.yaml \
  --parameters ParameterKey=KeyName,ParameterValue=key \
  ParameterKey=EnvType,ParameterValue=dev
```

Delete stack
```bash
aws cloudformation delete-stack \
  --stack-name MyTemplate
```


