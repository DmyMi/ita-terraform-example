#!/bin/bash
# A script that runs the app. It expects certain variables to be set via Terraform interpolation.

set -e

# Send the log output from this script to user-data.log, syslog, and the console
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

echo "Installing docker and starting app on port ${port} and piping all log output to syslog"
sudo yum update -y
sudo yum install docker -y
sudo service docker start
sudo docker run -p "${port}:${port}" registry.gitlab.com/dmymi/ita-terraform-example:latest 2>&1 | logger &

