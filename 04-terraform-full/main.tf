# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# This shows an example of how to use a Terraform module.
module "example_nodejs_app" {
  # The source field can be a path on your file system or a Git URL
  source = "./example-module"

  # Pass parameters to the module
  name          = "Example NodeJS App"
  port          = 8080
  key_pair_name = var.key_pair_name
}

