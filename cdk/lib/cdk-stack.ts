import * as ec2 from "@aws-cdk/aws-ec2"
import * as cdk from '@aws-cdk/core';

export class CdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = new ec2.Vpc(this, 'VPC', {
      cidr: "10.0.0.0/16"
    });

    // Latest Amazon Linux AMI
    const machineImage = ec2.MachineImage.latestAmazonLinux({
      generation : ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
      edition :  ec2.AmazonLinuxEdition.STANDARD,
      virtualization : ec2.AmazonLinuxVirt.HVM,
      storage : ec2.AmazonLinuxStorage.GENERAL_PURPOSE,
    });

    new ec2.Instance(this, id, {
      instanceType : new ec2.InstanceType('t2.micro'),
      machineImage,
      vpc,
    });
  }
}
