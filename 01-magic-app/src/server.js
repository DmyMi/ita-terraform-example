const http = require('http');

const server = http.createServer((req, res) => {
  const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
  res.writeHead(200, {"Content-Type": "text/plain"});
  res.end(`Hello, ${ip}!\n`);
});

server.listen(8080);

console.log("Server running at http://127.0.0.1:8080/");

process.on('SIGINT', function() {
  process.exit();
});