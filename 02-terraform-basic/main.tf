# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Create an EC2 instance
resource "aws_instance" "example" {
  # AMI ID for Amazon Linux AMI 2018.03.0 (HVM)
  ami                    = "ami-02354e95b39ca8dec"
  instance_type          = "t2.micro"

  tags = {
    Name      = "Example"
    ita_group = "Kv-045"
  }

  volume_tags = {
    ita_group = "Kv-045"
  }
}

