# Terraform example

1. Install [Terraform](https://www.terraform.io/).
2. Add your AWS credentials as the environment variables `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.
3. Run `terraform plan`.
4. If the plan looks good, run `terraform apply`.

## Cleaning up

To clean up the resources created by these templates, just run `terraform destroy`.