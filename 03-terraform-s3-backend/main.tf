# Configure the Terraform backend
terraform {
  backend "s3" {
    # Be sure to change this bucket name and region to match an S3 Bucket you have already created!
    bucket = "dmin1-state-store"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

# Get the latest Amazon Linux AMI
data "aws_ami" "amzn_linux" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*"]
  }
}

# Get IP of Terraform execuion instance/laptop
data "http" "myip" {
  url = "https://ipv4.icanhazip.com"
}

# Create an EC2 instance
resource "aws_instance" "example" {
  ami           = data.aws_ami.amzn_linux.image_id
  instance_type = "t2.micro"
  key_name      = var.key_pair_name

  vpc_security_group_ids = [aws_security_group.example.id]

  tags = {
    Name      = "Example"
    ita_group = "Kv-045"
  }

  volume_tags = {
    ita_group = "Kv-045"
  }
}

resource "aws_security_group" "example" {
  name = "example-sg"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}